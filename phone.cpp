#include <iostream>
#include <string>
using namespace std;


int main()
{
    //"02084116028"
    string input, refinedInput;
    
    cout << "Enter the phone number: ";
    cin >> input;
    refinedInput = ( "(" + input.substr(0, 3) + ") " ) + input.substr(3, 4) + " " + input.substr(7, 4);
    
    cout << "The formatted phone number is : " << refinedInput << endl;
    return 0;
}
