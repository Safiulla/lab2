#include <iostream>
using namespace std;

int main()
{
    float price, tipPercentage, tip;
    
    cout << "Enter the price (pounds): ";
    cin >> price;
    
    cout << "Enter the tip percentage: ";
    cin >> tipPercentage;
    
    tip = (price / 100) * tipPercentage ;
    
    cout << "The tip is: " << tip << " pounds" << endl;
}
